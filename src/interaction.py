from __future__ import division
import numpy as np
import matplotlib
# The 'MacOSX' backend appears to have some issues on Mavericks.
import sys
if sys.platform.startswith('darwin'):
	matplotlib.use('TkAgg')
import matplotlib.pyplot as pl

def interaction(alpha, h, d):
	return 1 - alpha*np.exp((-1 / (2*(h**2))) * (d**2))

x =  np.linspace(0, 100, 500)
y = np.zeros((len(x), 3))
y[:,0] = interaction(0.8, 24, x)
y[:,1] = interaction(0.99, 24, x)
y[:,2] = interaction(0.99, 30, x)

pl.plot(x, y)
pl.ylabel('psi(d)')
pl.xlabel('Distance d')
pl.show()