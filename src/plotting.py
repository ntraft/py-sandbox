# import matplotlib
# # The 'MacOSX' backend appears to have some issues on Mavericks.
# import sys
# if sys.platform.startswith('darwin'):
# 	matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import threading


plt.ion()

x = np.arange(0, 4*np.pi, 0.1)
y = [np.sin(i) for i in x]
plt.subplot(1,2,1)
p1 = plt.plot(x, y, 'g-', linewidth=1.5, markersize=4)
plt.xlabel('Independent Variable')
plt.ylabel('Dependent Variable')
plt.subplot(1,2,2)
p2 = plt.plot(x, [i**2 for i in y], 'g-', linewidth=1.5, markersize=4)

plt.draw()
plt.clf()
# plt.cla()
# p1[0].remove()
# p2[0].remove()
plt.title('Some Fancy Plot')
plt.plot(x, [i**2*i+0.25 for i in y], 'r-', linewidth=1.5, markersize=4)
plt.draw()

raw_input("Press Enter to quit.")



# def plotter():
# 	x = np.arange(0, 4*np.pi, 0.1)
# 	y = [np.sin(i) for i in x]
# 	plt.plot(x, y, 'g-', linewidth=1.5, markersize=4)
# 	plt.plot(x, [i**2 for i in y], 'g-', linewidth=1.5, markersize=4)
# 	
# 	# If using ion(), you need to call one of the below.
# 	plt.draw()
# 	while True: pass
# # 	plt.pause(0.0001) # An alternative to calling draw().
# 	# If NOT using ion(), you need a blocking show():
# # 	plt.show()
# # 	plt.plot(x, [i**2*i+0.25 for i in y], 'r-', linewidth=1.5, markersize=4)
# # 	plt.show()
# 
# plot_thread = threading.Thread(target=plotter)
# plot_thread.daemon = True
# plot_thread.start()
# # raw_input("Press Enter to quit.")
# while True:
# 	pass
