"""
A script for calculating the complicated penalty and interest schedule for unfiled/unpaid U.S. taxes.
"""
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

import numpy as np

today = date.today()

amounts = {
    ("fed", 2012): 975,
    ("ma", 2012): 1371,
    ("fed", 2013): -618,
    ("ma", 2013): 238,
    ("fed", 2015): -2671,
    ("fed", 2016): -398,
    ("fed", 2017): -692,
    ("fed", 2018): 2705,
    ("pa", 2018): 9,
    ("local", 2018): -12,
    ("fed", 2019): 135896,
    ("fed", 2020): 24058,
    ("local", 2020): 3,
}

total = sum(list(amounts.values()))

# Calculate penalties as of Feb 2022.
# Source: https://www.irs.gov/applicable-federal-rates
intrates = {
    (2022, 1): 0.0044,
    (2021, 1): 0.0014,
    (2020, 6): 0.0018,
    (2020, 5): 0.0025,
    (2020, 4): 0.0091,
    (2020, 3): 0.0149,
    (2020, 1): 0.0158,
    (2019, 1): 0.0268,
    (2018, 1): 0.0166,
    (2017, 1): 0.0096,
    (2016, 1): 0.0075,
    (2015, 1): 0.0041,
    (2014, 1): 0.0025,
    (2013, 1): 0.0021,
    (2012, 1): 0.0019,
    (2011, 1): 0.0043,
}


def find_rate(month):
    for m, rate in intrates.items():
        if m <= month:
            return rate
    raise RuntimeError(f"Could not find rate for month: {month}")


def penalty_schedule(months_unpaid):
    monthly_penalties = np.zeros(months_unpaid)
    total_file_penalty = 0.0
    total_pay_penalty = 0.0
    for i in range(months_unpaid):
        # Pay either the fixed rate, or the amount needed to reach the max, whichever is less.
        file_penalty = max(0.0, min(0.045, .25 - total_file_penalty))
        total_file_penalty += file_penalty
        pay_penalty = max(0.0, min(0.005, .25 - total_pay_penalty))
        total_pay_penalty += pay_penalty
        monthly_penalties[i] = file_penalty + pay_penalty
    return monthly_penalties


def list_months_since(year):
    # Start on tax day.
    curr_date = date(year, 4, 15)
    months = []
    while curr_date < today:
        months.append((curr_date.year, curr_date.month))
        curr_date += relativedelta(months=1)
    return months


# Penalties as of today.
# NOTE: These probably need to be all compounded together rather than calculated separately. But this will be a good
# approximation anyway.
total_penalties = 0.0
for (kind, year), amt in amounts.items():
    if kind != "fed" or year != 2019:
        continue

    # How much penalty racked up per month.
    months_since = list_months_since(year)
    penalties = penalty_schedule(len(months_since)) * amt
    
    # Now accumulate principal and interest over time.
    principal = 0.0
    for month, penalty in zip(months_since, penalties):
        print(f"{month}: Principal grows from {principal:.2f} to {principal * (1.03 + find_rate(month)):.2f}. Penalty = {penalty:.2f}.")
        # Accrue interest.
        principal *= (1.03 + find_rate(month))
        # Add new penalty.
        principal += penalty
    
    total_penalties += principal


print(f"Owed: {total}")
print(f"Penalties: {total_penalties:.2f}")
print(f"Grand total: {total + total_penalties:.2f}")

