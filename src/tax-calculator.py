#!/usr/bin/python

from __future__ import division

from collections import namedtuple
import sys


Bracket = namedtuple('Bracket', ['rate', 'income'])

tax_brackets = [
    Bracket(10, 9525),
    Bracket(12, 38700),
    Bracket(22, 82500),
    Bracket(24, 157500),
    Bracket(32, 200000),
    Bracket(35, 500000),
    Bracket(37, sys.float_info.max),
]

def calctax(income):
    tax = 0.0
    for bracket in tax_brackets:
        tax += (bracket.rate / 100.0) * min(income, bracket.income)
        income -= bracket.income
        if income <= 0:
            break
    return tax


equity = 541295
salary = 209000
bonus = 21500
income = float(equity + salary + bonus)


print 'Total income for 2019:', income
print 'Taxes for 2019:       ', calctax(income)
print 'Take home:            ', income - calctax(income)
